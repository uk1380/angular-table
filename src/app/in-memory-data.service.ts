import { InMemoryDbService } from 'angular-in-memory-web-api';


export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const forsales = [{
        "id": 1,
        "service" : "aaa",
        "state":"vic",
        "contact":"qqq",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 2,
        "service" : "bbb",
        "state":"vic",
        "contact":"aaa",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 3,
        "service" : "ccc",
        "state":"qld",
        "contact":"aaa",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 4,
        "service" : "ddd",
        "state":"nsw",
        "contact":"aaa",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 5,
        "service" : "aaa",
        "state":"sa",
        "contact":"xxx",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 6,
        "service" : "aaa",
        "state":"nsw",
        "contact":"yyy",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 7,
        "service" : "aaa",
        "state":"news",
        "contact":"zzz",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 8,
        "service" : "aaa",
        "state":"news",
        "contact":"www",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 9,
        "service" : "aaa",
        "state":"news",
        "contact":"aaa",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 10,
        "service" : "aaa",
        "state":"news",
        "contact":"qqq",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 11,
        "service" : "aaa",
        "state":"news",
        "contact":"www",
        "price":1000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 12,
        "service" : "aaa",
        "state":"news",
        "contact":"aaa",
        "price":9999,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 13,
        "service" : "aaa",
        "state":"news",
        "contact":"aaa",
        "price":2000,
        "link":"https://www.google.com.au/"
    },
    {
        "id": 14,
        "service" : "aaa",
        "state":"news",
        "contact":"aaa",
        "price":3000,
        "link":"https://www.google.com.au/"
    }];
    return {forsales};
  }
}

