import { Component, OnInit, ViewChild } from '@angular/core';
import { AppService } from './app.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  globalFilter = '';
  serviceFilter = '';
  stateFilter = '';

  contactClicked = false;
  priceClicked = false;

  filteredData: any;

  colfilter = {
    service: this.serviceFilter,
    state: this.stateFilter
  };

  forsales: any;

  constructor(private appservice: AppService) { }


  @ViewChild(MatPaginator) paginator: MatPaginator;


  ngOnInit(): void {

    this.getAllForsales();

  }


  getAllForsales() {
    this.appservice.getForsales().subscribe(data => {

      this.filteredData = this.forsales = data;

    });
  }

  /**
   * filter all data
   * @param filter 
   */
  applyAllFilter(filter){
    this.filteredData = [];
    this.globalFilter = filter;

    this.forsales.forEach(element => {

      if (JSON.stringify(Object.values(element)).trim().includes(this.globalFilter.toLowerCase())){
        this.filteredData.push(element);
      }

    });
  }

  /**
   * single service filter
   * @param filter 
   */
  applyServiceFilter(filter) {
    this.filteredData = [];
    this.colfilter.service = filter

    this.forsales.forEach(element => {
      if (element.service.includes(this.colfilter.service) && element.state.includes(this.colfilter.state)) {
        this.filteredData.push(element);
      }

    });
  }

  /**
   * filter state 
   * @param filter 
   */
  applyStateFilter(filter) {
    this.filteredData = [];
    this.colfilter.state = filter;

    this.forsales.forEach(element => {
      if (element.service.includes(this.colfilter.service) && element.state.includes(this.colfilter.state)) {
        this.filteredData.push(element);
      }
    });
  }
  /**
   * sort by contract column 
   */
  sortContact(){
    if (this.contactClicked == false){
      this.filteredData.sort((a, b) => (a.contact > b.contact) ? 1 : -1);
      
      this.contactClicked = true;

    }else{
      this.filteredData.sort((a, b) => (a.contact > b.contact) ? -1 : 1);
      
      this.contactClicked = false;
    }
  }

  /**
   * sort by price column
   */
  sortPrice(){
    if (this.priceClicked == false){
      this.filteredData.sort((a, b) => (a.price > b.price) ? 1 : -1);
      
      this.priceClicked = true;

    }else{
      this.filteredData.sort((a, b) => (a.price > b.price) ? -1 : 1);
      
      this.priceClicked = false;
    }
  }

}





