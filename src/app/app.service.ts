import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AppService {
 forsales_url: string = '/api/forsales';
 constructor(private http: HttpClient) {}


//Gets all tasks
 getForsales() {
    return this.http.get(this.forsales_url).pipe(
        map((res) => {return res;}));
    }
 }


